//SOUNDS
var audio = [], typingText = [],
	audioPieces = [];

//TIMEOUTS
var timeout = [], timer;

//AWARDs NUMBER
var awardNum = 0;

//FUNCTIONS

var launch = [];

var startTimer = function(jqueryElement, secondsLeft, callBack)
{
	if(secondsLeft <= 0)
		callBack();
	else
	{
		secondsLeft --;
		var minutesLeft = Math.floor(secondsLeft / 60);
		var result = "";
		secondsLeft = Math.round(secondsLeft % 60);
		
		if(minutesLeft > 9) 
			result = "" + minutesLeft + ":";
		else
			result = "0" + minutesLeft + ":";
		
		if(secondsLeft > 9)
			result += secondsLeft;
		else 
			result += "0" + secondsLeft;
		
		jqueryElement.html(result);
		
		secondsLeft += (minutesLeft * 60);
		
		timer = setTimeout(function(){
			startTimer(jqueryElement, secondsLeft, callBack);
		}, 1000);
	}
}

var stopTimer = function()
{
	clearTimeout(timer);
}

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
	
	var currPiece = this,
		currAudio = this.audio;
	
	currAudio.addEventListener("timeupdate", function(){
		var currTime = Math.round(currAudio.currentTime);
		
		if(currTime == currPiece.end)
			currAudio.pause();
	});
}

AudioPiece.prototype.play = function()
{
	this.audio.currentTime = this.start;
	this.audio.play();
}

AudioPiece.prototype.pause = function()
{
	this.audio.pause();
}

AudioPiece.prototype.addEventListener = function(e, callBack)
{
	var currPiece = this,
		currAudio = this.audio,
		currDuration = Math.round(currAudio.duration);
	
	if(e === "ended")
	{
		console.log("else end: " + currPiece.end + ", " + currDuration);
		currAudio.addEventListener("timeupdate", function(){
			var currTime = Math.round(currAudio.currentTime);
				
			if(currTime === currPiece.end)
			{
				currAudio.pause();
				currAudio.currentTime += 3;
				callBack();
			}
		});
	}
	if(e === "timeupdate")
	{
		currPiece.audio.addEventListener("timeupdate", callBack);
	}
}

AudioPiece.prototype.getStart = function()
{
	return this.start;
}

var allHaveHtml = function(jqueryElement){
	for(var i = 0; i < jqueryElement.length; i ++)
	{
		if(!$(jqueryElement[i]).html())
			return false;
	}
	return true;
}

var fadeOneByOne = function(jqueryElement, curr, interval, endFunction)
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			fadeOneByOne(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'n')
		{
			tObj.jqueryElement.append("<br>");
			tObj.currentSymbol += 2;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 's')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<sup>" + tObj.text[tObj.currentSymbol] + "</sup>");
			tObj.currentSymbol ++;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'b')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<b>" + tObj.text[tObj.currentSymbol] + "</b>");
			tObj.currentSymbol ++;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
			tObj.write();
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum);
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, interval, times)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("outline", "3px solid red");
		timeout[1] = setTimeout(function(){
			jqueryElements.css("outline", "");
			if (times) 
				blink(jqueryElements, interval, --times)		
		}, intervalHalf);
	}, intervalHalf);
}

launch["frame-000"] = function(){
	
}

launch["frame-101"] = function(){
	theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		cloud = $(prefix + ".cloud");
	
	audio[0] = new Audio("audio/nature.mp3"),
	audio[1] = new Audio("audio/elevator.mp3"),
	audio[2] = new Audio("audio/s1-1.mp3");
	
	doneSecond = 0;
		
	cloud.fadeOut(0);
	bg.fadeOut(0);
	fadeNavsIn();
	
	bg.addClass("transition-2s");
	
	var functions = [];
	
	functions[0] = function(){
		audio[0].volume = 0.2;
		bg.css("background-position", "3% 80%");
		bg.css("background-size", "500%")
	};
	
	functions[2] = function(){
		bg.css("background-position", "40% 80%");
		bg.css("background-size", "300%");
	};
		
	functions[5] = function(){
		cloud.fadeOut(0);
		bg.css("background-position", "99% 15%");
		bg.css("background-size", "450%");
	};
	functions[6] = function(){
		bg.css("background-position", "72% 25%");
		bg.css("background-size", "500%");
	};
	
	audio[0].addEventListener("timeupdate", function(){
		var currTime = Math.round(this.currentTime);
		if(doneSecond !== currTime)
		{
			doneSecond = currTime;
			
			switch(doneSecond)
			{
				case 4: functions[0]();
						break;
				case 5: audio[1].currentTime = 0;
						audio[1].play();
						break;
				case 9: functions[2]();
						break;
				case 13: audio[0].volume = 0.1; 
						audio[2].currentTime = 0;
						audio[2].play();
						cloud.fadeIn(500);
						break;
				case 21: functions[5]();
						break;
				case 25: functions[6]();
						break;
				case 28: fadeNavsIn();
						break;
			}
		}
	});

	startButtonListener = function(){
		audio[0].play();
		bg.fadeIn(1000);
	}
}

launch["frame-102"] = function(){
	theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		processes = $(prefix + ".process"),
		playButtons = $(prefix + ".process .fa-play");	
		
	audio[0] = new Audio("audio/s2-1.mp3");
	
	processes.fadeOut(0);
	bg.fadeOut(0);
	fadeNavsIn();
	
	audioPiece = {};
	audioPiece["condensation-rus"] = new AudioPiece("s2-1", 0, 4);
	audioPiece["condensation-kaz"] = new AudioPiece("s2-1", 0, 4);
	audioPiece["condensation-eng"] = new AudioPiece("s2-1", 0, 4);
	
	audioPiece["evaporation-rus"] = new AudioPiece("s2-1", 4, 8);
	audioPiece["evaporation-kaz"] = new AudioPiece("s2-1", 4, 8);
	audioPiece["evaporation-eng"] = new AudioPiece("s2-1", 4, 8);
	
	audioPiece["freezing-rus"] = new AudioPiece("s2-1", 7, 12);
	audioPiece["freezing-kaz"] = new AudioPiece("s2-1", 7, 12);
	audioPiece["freezing-eng"] = new AudioPiece("s2-1", 7, 12);
	
	audioPiece["boiling-rus"] = new AudioPiece("s2-1", 12, 16);
	audioPiece["boiling-kaz"] = new AudioPiece("s2-1", 12, 16);
	audioPiece["boiling-eng"] = new AudioPiece("s2-1", 12, 16);
	
	audioPiece["cooling-rus"] = new AudioPiece("s2-1", 17, 23);
	audioPiece["cooling-kaz"] = new AudioPiece("s2-1", 17, 23);
	audioPiece["cooling-eng"] = new AudioPiece("s2-1", 17, 23);
	
	audioPiece["heat-rus"] = new AudioPiece("s2-1", 22, 27);
	audioPiece["heat-kaz"] = new AudioPiece("s2-1", 22, 27);
	audioPiece["heat-eng"] = new AudioPiece("s2-1", 22, 27);
	
	audioPiece["temperature-rus"] = new AudioPiece("s2-1", 27, 35);
	audioPiece["temperature-kaz"] = new AudioPiece("s2-1", 27, 35);
	audioPiece["temperature-eng"] = new AudioPiece("s2-1", 27, 35);
	
	audio[0].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 3000);
	});
	
	var playButtonsListener = function(){
		var parent = $(this).parent();
		audioPiece[parent.attr("id")].play();
	};
	playButtons.off("click", playButtonsListener);
	playButtons.on("click", playButtonsListener);
	
	startButtonListener = function(){
		bg.fadeIn(1000);
		timeout[1] = setTimeout(function(){
			audio[0].play();
			fadeOneByOne(processes, 0, 1200, function(){
			});
		}, 2000);
	};
}

launch["frame-103"] = function(){
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		processes = $(prefix + ".process"),
		title = $(prefix + ".title");
	
	fadeNavsIn();
	bg.fadeOut(0);
		
	title.fadeOut(0);
	
	audio[0] = new Audio("audio/water-droplet.mp3");
	audio[1] = new Audio("audio/water-splash.mp3");
	audio[2] = new Audio("audio/wood-cracking.mp3");
	
	audioPieces[0] = new AudioPiece("s3-1", 0, 8);
	audioPieces[1] = new AudioPiece("s3-1", 11, 20);
	
	var processesListener = function(){
		audio[2].play();
		$(this).toggleClass("active");
		
		var activeSpans = $(".active");
		
		if (activeSpans.length === 3)
		{
			dataProcess = [$(activeSpans[0]).attr("data-process"), $(activeSpans[1]).attr("data-process"),
							$(activeSpans[2]).attr("data-process")];
			if((dataProcess[0] === dataProcess[1]) && (dataProcess[1] === dataProcess[2]))
			{
				audio[0].play();
				activeSpans.remove()
				
				if(!$(prefix + ".process").length)
				{
					audioPieces[1].play();
					fadeNavsIn();
				}	
			}
			else
			{
				activeSpans.removeClass("active");
				activeSpans.addClass("error");
				audio[1].play();
				setTimeout(function(){activeSpans.removeClass("error");}, 1000);
			}				
		}
	};
	processes.off("click", processesListener);
	processes.on("click", processesListener);
	processes.fadeOut(0);
	audioPieces[0].addEventListener("ended", function(){
		title.fadeIn(1000);
		processes.fadeIn(1000);
	});
	
	startButtonListener = function(){
		bg.fadeIn(1000);
		timeout[0] = setTimeout(function(){
			audioPieces[0].play();
		}, 2000);
	};
};

launch["frame-104"] = function(){
	theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		balls = $(prefix + ".ball");
		
	audio[0] = new Audio("audio/water-droplet.mp3");
	audio[1] = new Audio("audio/water-splash.mp3");
	audio[2] = new Audio("audio/wood-cracking.mp3");
	audioPieces[0] = new AudioPiece("s4-1", 0, 3);
		
	balls.fadeOut(0);
	bg.fadeOut(0);
	fadeNavsIn();
	
	var ballsListener = function(){
		audio[2].play();
	};
	balls.off("click", ballsListener);
	balls.on("click", ballsListener);
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	}
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		basket.css("background-color", vegetable.css("background-color"));
		basket.css("color", "white");
		vegetable.remove();
		audio[0].play();
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css({
			"left": "", 
			"top": ""
		});
		audio[1].play();
	}
	
	var finishCondition = function()
	{
		return !$(prefix + ".ball").length;
	}
	
	var finishFunction = function(){
		timeout[0] = setTimeout(function(){
			fadeNavsIn(0);
		}, 1000);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	startButtonListener = function(){
		audioPieces[0].play();
		balls.fadeIn(1000);
		bg.fadeIn(1000);
	}
};

launch["frame-105"] = function(){
	theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base"),
		balls = $(prefix + ".ball");
		
	audio[0] = new Audio("audio/water-droplet.mp3");
	audio[1] = new Audio("audio/water-splash.mp3");
	audio[2] = new Audio("audio/wood-cracking.mp3");
	audioPieces[0] = new AudioPiece("s4-1", 3, 8);
		
	balls.fadeOut(0);
	bg.fadeOut(0);
	fadeNavsIn();
	
	var ballsListener = function(){
		audio[2].play();
	};
	balls.off("click", ballsListener);
	balls.on("click", ballsListener);
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	}
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		basket.css("background-color", vegetable.css("background-color"));
		basket.css("color", "white");
		vegetable.remove();
		audio[0].play();
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css({
			"left": "", 
			"top": ""
		});
		audio[1].play();
	}
	
	var finishCondition = function()
	{
		return !$(prefix + ".ball").length;
	}
	
	var finishFunction = function(){
		timeout[0] = setTimeout(function(){
			fadeNavsIn(0);
		}, 1000);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	startButtonListener = function(){
		audioPieces[0].play();
		balls.fadeIn(1000);
		bg.fadeIn(1000);
	}
};

launch["frame-106"] = function(){
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		medeo = $(prefix + ".bg-base"),
		iceSkater = $(prefix + ".ice-skater"),
		cloudAndRain = $(prefix + ".cloud-and-rain"),
		medeoEvaporation = $(prefix + ".medeo-evaporation"),
		matter = $(prefix + ".elem"),
		sugar = $(prefix + ".sugar"), 
		shampoo = $(prefix + ".shampoo"),
		juice = $(prefix + ".juice"),
		juiceAnimated = $(prefix + ".juice-animated"), 
		oil = $(prefix + ".oil"), 
		gas = $(prefix + ".gas"),
		gasSpread = $(prefix + ".gas-spread");
	
	audio[0] = new Audio("audio/matter.mp3");
	audio[1] = new Audio("audio/rain.mp3");
	audio[2] = new Audio("audio/ice-skating.mp3");
	
	var currentSecond = 0;
	
	audio[0].addEventListener("timeupdate", function(){
		var currTime = Math.round(this.currentTime);
		if(currentSecond !== currTime)
		{
			currentSecond = currTime;
			
			switch(currentSecond)
			{
				case 12:setTimeout(function(){
						audio[0].pause(); 
						audio[2].play();
						medeo.css("backgroundSize", "400%");
						medeo.css("backgroundPosition", "50% 90%");

						setTimeout(function()
						{
							iceSkater.fadeIn(0);
							iceSkater.css("backgroundPosition", "70% 70%");
						}, 1000);
					}, 200); 
					break;
				case 13: audio[0].pause();
						cloudAndRain.fadeIn(0);
						cloudAndRain.css("opacity", "0.5");
						setTimeout(function(){
							audio[1].play();
						}, 2000)
						break;
				case 14: setTimeout(function(){
							audio[0].pause(); 
						}, 200); 
						medeo.css("backgroundSize", "350%");
						medeo.css("backgroundPosition", "70% 105%");
						
						medeoEvaporation.css("backgroundSize", "350%");
						medeoEvaporation.css("backgroundPosition", "80% 95%");
						setTimeout(function(){
							medeoEvaporation.fadeIn(300);	
						}, 1000);
						setTimeout(function(){
							audio[0].play();
							matter.fadeOut(1000);
						}, 2000);
						break;
				case 16: sugar.fadeIn(300);
						break;
				case 20: sugar.css("background-position", "-150% 0%"); 
						shampoo.fadeIn(1000);
						break;
				case 22: shampoo.css("background-position", "0% 0%") 
						oil.fadeIn(300);
						break;
				case 23: oil.fadeIn(0);
						break;
				case 24: juice.fadeIn(0);
						juice.css("background-position", "95% 0%");
						break;
				case 25: oil.fadeOut(0);
						 shampoo.fadeOut(0);
						 juice.css("background-position", "20% 0%");
						setTimeout(function(){
							juice.fadeOut(0);
							juiceAnimated.css("background-image", "url('pics/s6-juice-animated.gif')");
							juiceAnimated.fadeIn(0);
						}, 1000);						
						break;
				case 30: juiceAnimated.fadeOut(200);
						gas.fadeIn(0);
						var gasImg = new Image();
						gasImg.src = "pics/gas.gif";
						gasImg.addEventListener("load", function(){
							gas.css("background-image", "url('pics/s6-gas.gif')");
						});
						break;
				case 36: 
						 gas.fadeOut(0);
						 gasSpread.fadeIn(0);
						 var gasSpreadImg = new Image();
						 gasSpreadImg.src = "pics/s6-gas-spread.gif";
						 gasSpreadImg.addEventListener("load", function(){
							gasSpread.css("background-image", "url('pics/s6-gas-spread.gif')")
						 });
						 break;
			}
		}
	});
	audio[0].addEventListener("ended", function(){
		fadeNavsIn();
	});
	
	audio[1].addEventListener("ended", function(){
		audio[0].play();
		cloudAndRain.fadeOut();
	});
	audio[2].addEventListener("ended", function(){
		audio[0].play();
		iceSkater.fadeOut(0);
		medeo.css("backgroundSize", ""); 
		medeo.css("backgroundPosition", "");
	});
	
	matter.fadeOut(0);
	medeo.fadeIn(0);
	fadeNavsIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
		}, 1000);
	}
}

launch["frame-107"] = function(){
	theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		coolGraph = $(prefix + ".cool.graph"),
		heatGraph = $(prefix + ".heat.graph"),
		coolTitle = $(prefix + ".cool.title"),
		heatTitle = $(prefix + ".heat.title");
		
	var coolGraphSprite = new Motio(coolGraph[0], {
		"fps": 1,
		"frames": 9
	});
	coolGraphSprite.on("frame", function(){
		if(this.frame === this.frames - 1 || this.frame === 6)
			this.pause();
	});
	var heatGraphSprite = new Motio(heatGraph[0], {
		"fps": "1",
		"frames": "10"
	});
	heatGraphSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	audioPieces[0] = new AudioPiece("s6-1", 0, 12);
	audioPieces[1] = new AudioPiece("s6-1", 16, 31);
	audioPieces[2] = new AudioPiece("s6-1", 35, 43);
	audioPieces[3] = new AudioPiece("s6-1", 46, 999);
	
	audioPieces[0].addEventListener("ended", function(){
		audioPieces[1].play();
		heatTitle.fadeIn(2000);
		heatGraph.fadeIn(2000);
		timeout[0] = setTimeout(function(){
			heatGraphSprite.play();
		}, 2000);
	});
	audioPieces[1].addEventListener("ended", function(){
		audioPieces[2].play();
		heatTitle.fadeOut(2000);
		heatGraph.fadeOut(2000);
		coolTitle.fadeIn(2000);
		coolGraph.fadeIn(2000);
		timeout[0] = setTimeout(function(){
			coolGraphSprite.play();
		}, 2000);
	});
	
	audioPieces[2].addEventListener("ended", function(){
		audioPieces[3].play();
		coolGraphSprite.play();
		timeout[1] = setTimeout(function(){
			fadeNavsIn();
		}, 10000);
	});
	
	coolGraph.fadeOut(0);
	heatGraph.fadeOut(0);
	coolTitle.fadeOut(0);
	heatTitle.fadeOut(0);
	fadeNavsIn();
	
	startButtonListener = function(){
		audioPieces[0].play();
	}
};

launch["frame-108"] = function(){
	theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		graph = $(prefix + ".graph"),
		tasks = $(prefix + ".task"),
		answers = $(prefix + ".answer"),
		activeTask = 1, 
		correctNum = 0, 
		result = $(prefix + ".task-4 .question");
	
	audioPieces[0] = new AudioPiece("s7-1", 0, 3);
	audioPieces[1] = new AudioPiece("s7-1", 4, 8);
	audioPieces[2] = new AudioPiece("s7-1", 9, 13);
	audioPieces[3] = new AudioPiece("s7-1", 14, 99);
	
	audioPieces[0].addEventListener("ended", function(){
		audioPieces[1].play();
		$(".task-" + activeTask).fadeIn(2000);
	});
	
	var answersListener = function(){
		var currButton = $(this);
		if(currButton.hasClass("correct"))
		{
			currButton.css("background-color", "green");
			correctNum ++;
		}
		else
		{
			currButton.css("background-color", "red");
			$(".task-" + activeTask + " .correct").css("background-color", "green");
		}
		
		result.html("У вас " + correctNum + " правильных ответа.");
		
		timeout[0] = setTimeout(function(){
			activeTask ++;
			tasks.fadeOut(1000);
			$(".task-" + activeTask).fadeIn(1000);
			
			if(activeTask === 4)
				timeout[1] = setTimeout(function(){
					fadeNavsIn();
				}, 3000);
			else
				audioPieces[activeTask].play();
		}, 4000);
	}
	answers.off("click", answersListener);
	answers.on("click", answersListener);
	
	tasks.fadeOut(0);
	graph.fadeOut(0);
	fadeNavsIn();
	
	startButtonListener = function(){
		audioPieces[0].play();
		graph.fadeIn(1000);
	}
}

launch["frame-109"] = function(){
	theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
			task = $(prefix + ".task"),
			sketchpadContainer = $(prefix + ".sketchpad-container"),
			sketchpad = $(prefix + ".responsive-sketchpad").sketchpad(
			{
				aspectRatio: 2/1            // (Required) To preserve the drawing, an aspect ratio must be specified
			});	
	
	sketchpad.attr("width", sketchpadContainer.css("width"));
	sketchpad.attr("height", sketchpadContainer.css("height"));
	$(window).resize(function(){
		sketchpad.attr("width", sketchpadContainer.css("width"));
		sketchpad.attr("height", sketchpadContainer.css("height"));
	});
	
	sketchpad.setLineColor('#177EE5');
	sketchpad.setLineSize(2);
	
	$(".control").click(function(){
		switch($(this).attr("id"))
		{
			case "clear":
				sketchpad.clear();
			break;
			case"undo":
				sketchpad.undo();
			break;
			case "red":
				sketchpad.setLineColor('#D5000D');
			break;
			case "blue":
				sketchpad.setLineColor('#177EE5');
			break;
			case "download":
			var canvas = document.querySelector("#frame-109 .responsive-sketchpad");
			  var context = canvas.getContext("2d");
			  context.fillStyle = "green";
			  window.open(canvas.toDataURL("image/png"), '_blank');
			  showNavBar();
			break;
		}
	});
	
	task.fadeOut(0);
	fadeNavsIn();
	
	startButtonListener = function(){
		task.fadeIn(1000);
	};
}

launch["frame-110"] = function(){
	theFrame = $("#frame-110"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		elems = $(prefix + ".elem"),
		molecules = $(prefix + ".molecule"),
		glasses = $(prefix + ".glass"),
		waterGlass = $(prefix + ".glass.water"),
		iceGlass = $(prefix + ".glass.ice"),
		vaporGlass = $(prefix + ".glass.vapor"),
		iceStructure = $(prefix + ".ice.structure"),
		waterStructure = $(prefix + ".water.structure"),
		vaporStructure = $(prefix + ".vapor.structure"),
		structures = $(prefix + ".structure");
		
	audioPieces[0] = new AudioPiece("s9-1", 0, 14);
	audioPieces[1] = new AudioPiece("s9-1", 17, 26);
	audioPieces[2] = new AudioPiece("s9-1", 30, 39);
	audioPieces[3] = new AudioPiece("s9-1", 57, 70);
	audioPieces[4] = new AudioPiece("s9-1", 74, 99);
	
	fadeNavsIn();
	molecules.fadeOut(0);
	glasses.fadeOut(0);
	
	audioPieces[0].addEventListener("ended", function(){
		molecules.fadeOut(1000);
		audioPieces[1].play();
	});
	
	audioPieces[1].addEventListener("ended", function(){
		audioPieces[2].play();
		timeout[2] = setTimeout(function(){
			iceGlass.fadeIn(1000);
		}, 1000);
		timeout[3] = setTimeout(function(){
			iceGlass.addClass("big");
		}, 4000);
		timeout[4] = setTimeout(function(){
			iceStructure.addClass("big");
		}, 6000);		
	});
	audioPieces[2].addEventListener("ended", function(){
		audioPieces[3].play();
		iceGlass.fadeOut(500);
		iceStructure.fadeOut(500);
		timeout[5] = setTimeout(function(){
			waterGlass.fadeIn(1000);
		}, 1000);
		timeout[6] = setTimeout(function(){
			waterGlass.addClass("big");
		}, 4000);
		timeout[7] = setTimeout(function(){
			waterStructure.addClass("big");
		}, 6000);	
	});
	audioPieces[3].addEventListener("ended", function(){
		audioPieces[4].play();
		waterGlass.fadeOut(500);
		waterStructure.fadeOut(500);
		timeout[8] = setTimeout(function(){
			vaporGlass.fadeIn(1000);
		}, 1000);
		timeout[9] = setTimeout(function(){
			vaporGlass.addClass("big");
		}, 4000);
		timeout[10] = setTimeout(function(){
			vaporStructure.addClass("big");
		}, 6000);
		timeout[11] = setTimeout(function(){
			fadeNavsIn();
		}, 13000);		
	});
	startButtonListener = function(){
		audioPieces[0].play();
		timeout[0] = setTimeout(function(){
			molecules.fadeIn(1000);
		}, 2000);
		timeout[1] = setTimeout(function(){
			molecules.addClass("center");
		}, 5000);
	};
}

launch["frame-111"] = function(){
	theFrame = $("#frame-111"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		stateChange = $(prefix + ".state-change");

	audioPieces[0] = new AudioPiece("s10-1", 0, 6);
	audioPieces[1] = new AudioPiece("s10-1", 10, 99);
	
	audioPieces[0].addEventListener("ended", function(){
		audioPieces[1].play();
		timeout[1] = setTimeout(function(){
			fadeNavsIn();
		}, 20000);
	});
	
	var stateChangeSprite = new Motio(stateChange[0], {
		"fps": "1",
		"frames": "15"
	});
	
	stateChangeSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	stateChange.fadeOut(0);
	fadeNavsIn();
	
	startButtonListener = function(){
		stateChange.fadeIn(1000);
		audioPieces[0].play();
		timeout[0] = setTimeout(function(){
			stateChangeSprite.play();
		}, 2000);
	}
};

launch["frame-112"] = function(){
	theFrame = $("#frame-112"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		balls = $(prefix + ".ball"),
		baskets = $(prefix + ".basket");

	audio[0] = new Audio("audio/s11-1.mp3");
		
	var successCondition = function(vegetable, basket){
		return vegetable === basket
	}
	
	var successFunction = function(vegetable, basket){
		vegetable.remove();
		basket.css("border-color", "green");
	}
	
	var failFunction = function(vegetable, basket){
		vegetable.css({
			"top": "", 
			"left": ""
		});
	}
	
	var finishCondition = function(){
		return !$(prefix + ".ball").length
	}
	
	var finishFunction = function(){
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	fadeNavsIn();
	
	startButtonListener = function(){
		audio[0].play();
		timeout[0] = setTimeout(function(){
			blink(balls, 500, 3)
		}, 1000);
		timeout[0] = setTimeout(function(){
			blink(baskets, 500, 3)
		}, 3000);
	}
}

launch["frame-113"] = function(){
	theFrame = $("#frame-113"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		videoContainer = $(prefix + ".video-container"),
		video = $(prefix + ".video");
		
	audio[0] = new Audio("audio/s12-1.mp3");
	
	videoContainer.fadeOut(0);
	fadeNavsIn();
	
	$(window).resize(function(){
		video.attr("width", videoContainer.css("width"));
		video.attr("height", videoContainer.css("height"));
	});
	
	video[0].addEventListener("ended", function(){
		fadeNavsIn();
	});
	
	startButtonListener = function(){
		videoContainer.fadeIn(500);
		video.attr("width", videoContainer.css("width"));
		video.attr("height", videoContainer.css("height"));
		video[0].play();
		audio[0].play();
	}
}

launch["frame-114"] = function(){
	theFrame = $("#frame-114"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		thermometer = document.querySelector(prefix + ".thermometer-animated"), 
		moleculesOn = document.querySelector(prefix + ".molecules-animated-on"),
		moleculesOff = document.querySelector(prefix + ".molecules-animated-off"),
		teapotOn = document.querySelector(prefix + ".teapot-on"),
		teapotOff = document.querySelector(prefix + ".teapot-off"),
		teapotAnimated = document.querySelector(prefix + ".teapot-animated"),
		turnOnButton = document.querySelector(prefix + ".turn-on-button"),
		turnOffButton = document.querySelector(prefix + ".turn-off-button"),
		allParts = $(".elem");
	
	allParts.fadeOut(0);
	$(thermometer).fadeIn(0);
	$(teapotOff).fadeIn(0);
	$(moleculesOff).fadeIn(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/kettle-boil-2.mp3");
	
	var sprtTh = new Motio(thermometer, {
		"fps": "3", 
		"frames": "12"
	});
	sprtTh.on("frame", function(){
		if(sprtTh.frame === sprtTh.frames - 1)
		{
			sprtTh.pause();
			$(teapotOn).fadeOut(0);
			$(teapotAnimated).fadeIn(0);
			$(moleculesOff).fadeOut(0);
			$(moleculesOn).fadeIn(0);
			sprtMOn = new Motio(moleculesOn, {
				"fps": "2", 
				"frames": "14"
			});
			sprtMOn.on("frame", function(){
				if(sprtMOn.frame === sprtMOn.frames - 1)
					sprtMOn.pause();
			});
			$(moleculesOn).css("background-size", (sprtMOn.frames * 100) + "% 100%");
			sprtMOn.play();
		}
	});
	$(thermometer).css("background-size", (sprtTh.frames * 100) + "% 100%");

	$(moleculesOff).css("background-size", "100% 100%");
	
	$(teapotAnimated).css("background-size", "100% 100%");
	
	$(teapotOff).css("background-size", "100% 100%");
	$(teapotOn).css("background-size", "100% 100%");
	
	audio[0].addEventListener("ended", function(){
		sprtTh.on("frame", function(){
			if(sprtTh.frame === 1)
			{
				sprtTh.pause();
				$(teapotAnimated).fadeOut(0);
				$(teapotOff).fadeIn(0);
			}
		});
		sprtTh.play(true);
		fadeNavsIn();
	});
	
	startButtonListener = function(){
		audio[0].play();
		$(teapotOff).fadeOut(0);
		$(teapotOn).fadeIn(0);
		sprtTh.play();
	};
}

launch["frame-115"] = function(){
	theFrame = $("#frame-115"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		balls = $(prefix + ".handle"),
		title = $(prefix + ".title");
	
	audio[0] = new Audio("audio/s13-1.mp3");
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		basket.css("background-color", vegetable.css("background-color"));
		basket.css("color", vegetable.css("color"));
		
		vegetable.remove();
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css({
			"left": "",
			"top": ""
		});
	}
	
	var finishCondition = function()
	{
		return !$(".handle").length;
	}
	
	var finishFunction = function()
	{
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	balls.fadeOut(0);
	fadeNavsIn();
	title.fadeOut(0);
	
	startButtonListener = function(){
		audio[0].play();
		balls.fadeIn(1000);
		title.fadeIn(1000);
	}
}

launch["frame-116"] = function(){
	theFrame = $("#frame-116"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		balls = $(prefix + ".ball"),
		girl = $(prefix + ".girl");
	
	audioPieces[0] = new AudioPiece("s14-1", 0, 6);
	audioPieces[1] = new AudioPiece("s14-1", 10, 99);
	
	audioPieces[0].addEventListener("ended", function(){
		balls.fadeIn(1000);
	});
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		basket.css("border", "none");
		vegetable.remove();
	};
	
	var failFunction = function(vegetable, basket){
		vegetable.css({
			"left": "",
			"top": ""
		});
	};
	
	var finishCondition = function(){
		return !$(prefix + ".ball").length;
	};
	
	var finishFunction = function(){
		audioPieces[1].play();
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 10000);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	girl.fadeOut(0);
	fadeNavsIn();
	balls.fadeOut(0);
	
	startButtonListener = function(){
		girl.fadeIn(1000);
		audioPieces[0].play();
	};
}

launch["frame-201"] = function(){
	theFrame = $("#frame-201"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		oldMen = $(prefix + ".old-man"),
		oldManSad = $(prefix + ".old-man.sad"),
		oldManHappy = $(prefix + ".old-man.happy"),
		oldManNeutral = $(prefix + ".old-man.neutral"),
		tasks = $(prefix + ".task"),
		activeTask = 1,
		answers = $(prefix + ".answer"),
		correctNum = 0,
		balls = $(prefix + ".ball"),
		answerfields = $(prefix + ".answerfield"),
		checkButton = $(prefix + ".check-button"),
		result = $(prefix + ".task-15 .question"),
		timer = $(prefix + ".timer");
		
	oldMen.fadeOut(0);
	oldManNeutral.fadeIn(0);
	tasks.fadeOut(0);
	fadeNavsIn();
	answerfields.val("");
	timer.html("");
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		vegetable.remove();
		basket.css("border", "5px solid green");
		oldManHappy.fadeIn(500);
		correctNum ++;
		result.html("У тебя " + correctNum + " правильных ответов.");
		
		timeout[0] = setTimeout(function(){
			oldManHappy.fadeOut(0);
			activeTask ++;
			tasks.fadeOut(0);
			$(".task-" + activeTask).fadeIn(0);
			blink($(".ball"), 500, 2);
		}, 3000);
	};
	
	var failFunction = function(vegetable, basket)
	{
		if(basket.hasClass("basket"))
		{
			vegetable.remove();
			basket.css("border", "5px solid red");
			$(".task-" + activeTask + " .correct").css("border", "5px solid green");
			oldManSad.fadeIn(500);
			
			timeout[0] = setTimeout(function(){
				oldManSad.fadeOut(0);
				activeTask ++;
				tasks.fadeOut(0);
				$(".task-" + activeTask).fadeIn(0);
				blink($(".ball"), 500, 2);
			}, 3000);
		}
	};
	
	var finishCondition = function()
	{
		
	};
	
	var finishFunction = function()
	{
		
	};
	
	var dragTask = DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction)
	
	var checkButtonListener = function(){
		answerfield = $(".task-" + activeTask + " .answerfield");
		
		for(var i = 0; i < answerfield.length; i++)
		{
			currField = $(answerfield[i]);
			
			console.log("currField.val() = " + currField.val());
			console.log("currField.attr('data-correct') = " + currField.attr("data-correct"));
			
			if(currField.val() === currField.attr("data-correct"))
			{
				currField.css("color", "green");
				correctNum ++;
				result.html("У тебя " + correctNum + " правильных ответов.");
			}
			else
			{
				currField.css("color", "red");
			}
		}
		timeout[0] = setTimeout(function(){
			activeTask ++;
			tasks.fadeOut(0);
			$(".task-" + activeTask).fadeIn(0);
			stopTimer();
		}, 3000);
	};
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	var answersListener = function(){
		currAnswer = $(this);
		if(currAnswer.hasClass("correct"))
		{
			oldManHappy.fadeIn(500);
			currAnswer.css("color", "green");
			correctNum ++;
			result.html("У тебя " + correctNum + " правильных ответов.");
		}
		else
		{
			oldManSad.fadeIn(500);
			currAnswer.css("color", "red");
			$(".task-" + activeTask + " .correct").css("color", "green");
		}
		
		timeout[0] = setTimeout(function(){
			oldManSad.fadeOut(0);
			oldManHappy.fadeOut(0);
			activeTask ++;
			tasks.fadeOut(0);
			$(".task-" + activeTask).fadeIn(0);
		}, 2000);
	};
	answers.off("click", answersListener);
	answers.on("click", answersListener);
	
	startButtonListener = function(){
		$(".task-" + activeTask).fadeIn(0);
		startTimer(timer, 120, function(){
			activeTask = 15;
			tasks.fadeOut(0);
			$(".task-" + activeTask).fadeIn(0);
		})
	};
};

launch["frame-301"] = function(){
	theFrame = $("#frame-301"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		text = $(prefix + ".text");
		
	text.fadeOut(0);
	fadeNavsIn();
	
	startButtonListener = function(){
		text.fadeIn(2000);
	};
}

launch["frame-401"] = function(){
	theFrame = $("#frame-401"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		facts = $(prefix + ".fact");
		
	var factsListener = function(){
		var currFact = $(this);
		if(currFact.hasClass("center"))
		{
			currFact.css("color", "");
			timeout[0] = setTimeout(function(){
				currFact.removeClass("center");
			}, 1000);
		}
		else
		{
			facts.removeClass("center");
			facts.css("color", "");
			timeout[0] = setTimeout(function(){
				currFact.addClass("center");
			}, 500);
			timeout[1] = setTimeout(function(){
				$(".center").css("color", "black");
			}, 1000);
		}
	};
	facts.off("click", factsListener);
	facts.on("click", factsListener);
	fadeNavsIn();
	
	startButtonListener = function(){
		blink(facts, 1000, 3);
	};
}

var hideEverythingBut = function(elem)
{
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	elemId = elem.attr("id");
	
	if(elemId === "frame-000")
		fadeNavsOut();
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	
	
	for (var i = 0; i < audioPieces.length; i++)
		audioPieces[i].pause();	

	for (var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);
	
	for (var i = 0; i < typingText.length; i++)
		typingText[i].stopWriting();
	
	if(elem.hasClass("fact"))
		$(".o2").fadeOut(0);
	
	launch[elemId]();
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link")), 
			elemId = elem.attr("id");
		
		launch[elemId]();
		
		hideEverythingBut(elem);
	});
};

var main = function()
{
	oxygenPointer = $(".o2-pointer");
	initMenuButtons();
	hideEverythingBut($("#frame-000"));
};

$(document).ready(main);